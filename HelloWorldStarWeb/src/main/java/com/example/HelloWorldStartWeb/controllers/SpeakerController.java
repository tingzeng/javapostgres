package com.example.HelloWorldStartWeb.controllers;

import com.example.HelloWorldStartWeb.models.Session;
import com.example.HelloWorldStartWeb.models.Speaker;
import com.example.HelloWorldStartWeb.repositories.SpeakerRepository;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/speakers")
public class SpeakerController {

  @Autowired
  private SpeakerRepository speakerRepository;


  @GetMapping
  public List<Speaker> list(){
    return speakerRepository.findAll();
  }

  @GetMapping
  @RequestMapping("{id}")
  public Speaker get(@PathVariable Long id){
    return speakerRepository.getOne(id);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Speaker Create(@RequestBody final Speaker speaker){
    return speakerRepository.saveAndFlush(speaker);
  }

  @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
  public void delete(@PathVariable Long id){
    speakerRepository.deleteById(id);
  }

  @RequestMapping(value="{id}", method = RequestMethod.PUT)
  @ResponseStatus(HttpStatus.ACCEPTED)
  public Speaker update(@PathVariable Long id, @RequestBody Speaker session){

    Speaker oldSpeaker = speakerRepository.getOne(id);

    if (oldSpeaker != null){

      BeanUtils.copyProperties(session, oldSpeaker, "session_id");

      return speakerRepository.saveAndFlush(oldSpeaker);
    }
    else
      return null;


  }

}
