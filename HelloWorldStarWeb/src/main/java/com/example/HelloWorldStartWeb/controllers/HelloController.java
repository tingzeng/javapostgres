package com.example.HelloWorldStartWeb.controllers;


import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

  @RequestMapping("/helloWorld")
  public String index() {
    return "Greetings from Spring Boot Second!";
  }

}
