package com.example.HelloWorldStartWeb;

import java.util.Arrays;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.ILoggerFactory;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HelloWorldStartWebApplication implements CommandLineRunner{

	private static final Logger log = LoggerFactory.getLogger(HelloWorldStartWebApplication.class);
	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(HelloWorldStartWebApplication.class, args);


// anothe test commit
/*	String[] beanNames = ctx.getBeanDefinitionNames();

		Arrays.sort(beanNames);

		for (String beanName : beanNames)
		{
			System.out.println(beanName);
		}*/
	}




	@Override
	public void run(String... args) throws Exception {
		log.info("Hello world application started.");
	}
}
