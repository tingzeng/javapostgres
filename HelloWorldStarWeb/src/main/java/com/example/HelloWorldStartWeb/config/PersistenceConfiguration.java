package com.example.HelloWorldStartWeb.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersistenceConfiguration {

  @Value("${postgres_DbUrl}")
  private String serverURL;

  @Value("${dbUsername}")
  private String userName;

  @Value("${dbPassword}")
  private String password;
  @Bean
  public DataSource dataSource(){
    DataSourceBuilder builder = DataSourceBuilder.create();
    builder.url(serverURL);
    builder.username(userName);
    builder.password(password);
    System.out.println("The custom datasource is being used");
    return builder.build();
  }

}
