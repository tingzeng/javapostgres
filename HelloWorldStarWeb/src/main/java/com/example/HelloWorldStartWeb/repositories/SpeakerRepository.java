package com.example.HelloWorldStartWeb.repositories;

import com.example.HelloWorldStartWeb.models.Speaker;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpeakerRepository extends JpaRepository<Speaker, Long> {

}
