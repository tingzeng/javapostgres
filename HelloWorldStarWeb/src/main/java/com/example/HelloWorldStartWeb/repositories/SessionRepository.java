package com.example.HelloWorldStartWeb.repositories;

import com.example.HelloWorldStartWeb.models.Session;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionRepository extends JpaRepository<Session, Long> {

}
