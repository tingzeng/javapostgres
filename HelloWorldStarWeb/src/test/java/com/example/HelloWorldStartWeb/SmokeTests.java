package com.example.HelloWorldStartWeb;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.HelloWorldStartWeb.controllers.SessionController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SmokeTests {

  @Autowired
  private SessionController sessionController;

  @Test
  public void contexLoads() throws Exception {
    assertThat(sessionController).isNotNull();
  }
}
